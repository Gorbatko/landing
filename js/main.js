$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        responsive : {
            0: {
                items: 1,
            },
            480: {
                items: 3,

            },
            900: {
                items: 3,
            }
        }
    })
});


document.getElementById("burger-js").onclick = function() {open()};

function open() {
    document.getElementById("burger-menu-js").classList.toggle("show");
    document.getElementById("burger-menu__lines").classList.toggle("active");
}